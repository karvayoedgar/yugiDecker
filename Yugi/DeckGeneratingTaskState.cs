﻿using System;
using CE.Collections;
using YugiDecker;

namespace Yugi
{
	public class DeckGeneratingTaskState
	{
		public GenerationParameters Parameters { get; }
		public DeckRulesSet Rules { get; }
		public EvolutiveDeckOptimizer Optimizer { get; }
		public NamedDeck Deck { get; private set; }
		public Bag<ICard> Trunk { get; }
		public Deck CurrentFittest { get; private set; }
		public double CurrentFittestValue { get; private set; }

		public DeckGeneratingTaskState(GenerationParameters parameters, DeckRulesSet rules, Bag<ICard> trunk)
		{
			const int EVOL_POP_SIZE = 40; //= 2000;
			Parameters = parameters ?? throw new ArgumentNullException(nameof(parameters));
			Rules = rules ?? throw new ArgumentNullException(nameof(rules));
			Trunk = trunk ?? throw new ArgumentNullException(nameof(trunk));
			Optimizer = new EvolutiveDeckOptimizer(EVOL_POP_SIZE, Rules, Trunk);
		}

		public void Initialize()
		{
			CurrentFittest = Optimizer.GetFittest();
			_prevFittest = new Deck();
			_lastValue = double.NegativeInfinity;
		}

		public void Finish() { _isExiting = true; }

		public void SetupNew()
		{
			Optimizer.Populate();
		}

		public void Iterate()
		{
			const double FITTING_VALUE_EPSILON = 0.0000001d;
			while (!_isExiting)
			{
				var sortedDeck = Optimizer.GetSortedDeckArray();
				var nowHighestDeck = sortedDeck[sortedDeck.Length - 1];
				CurrentFittest = nowHighestDeck;
				var leastFit = sortedDeck[0];
				CurrentFittestValue = CurrentFittest.GetValue();
				if (CurrentFittestValue - _lastValue > FITTING_VALUE_EPSILON)
				{
					_lastValue = CurrentFittestValue;
					Breakthrough?.Invoke(this, EventArgs.Empty);
					Console.WriteLine("\r[{1:hh:mm:ss}] Current deck score: {0}", CurrentFittestValue, DateTime.Now);
				}
				Optimizer.Iterate();
				Iterated?.Invoke(this, EventArgs.Empty);
			}
		}

		public event EventHandler Breakthrough;
		public event EventHandler Iterated;

		bool _isExiting;
		Deck _prevFittest;
		double _lastValue;
	}
}