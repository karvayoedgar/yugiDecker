﻿using System;

namespace Yugi
{
	/// <summary>
	/// Event args referencing a deck.
	/// </summary>
	[Serializable]
	public sealed class DeckEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the deck.
		/// </summary>
		public NamedDeck Deck { get; }

		/// <param name="deck">Deck.</param>
		public DeckEventArgs(NamedDeck deck)
		{
			Deck = deck;
		}
	}
}