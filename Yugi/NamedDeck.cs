﻿using YugiDecker;

namespace Yugi
{
	/// <summary>
	/// A <see cref="Deck"/> with a name.
	/// </summary>
	public class NamedDeck : Deck
	{
		/// <summary>
		/// Gets or sets the name of this deck.
		/// </summary>
		public string Name { get; set; }
	}
}