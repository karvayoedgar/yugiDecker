﻿using System;
using YugiDecker;
using Gtk;

namespace Yugi
{
	/// <summary>
	/// A widget that displays, create and pick a deck.
	/// </summary>
	[System.ComponentModel.ToolboxItem(true)]
	public partial class DeckSelector : Bin
	{
		/// <summary>
		/// </summary>
		public DeckSelector()
		{
			Build();
			_deckStore = new ListStore(typeof(Deck), typeof(string));
			_deckComboBox.Model = _deckStore;
			var cr = new CellRendererText();
			_deckComboBox.PackStart(cr, true);
			_deckComboBox.AddAttribute(cr, "text", 1);
			ShowAll();
		}

		/// <summary>
		/// Add a deck to the combo box.
		/// </summary>
		/// <param name="deck">Deck.</param>
		/// <param name="select">If set to <c>true</c> this deck will be active in the combobox.</param>
		public void Add(NamedDeck deck, bool select = true)
		{
			_deckStore.AppendValues(ValueToArray(deck));
			ShowAll();
			if (select)
			{
				_deckComboBox.Active = _deckStore.IterNChildren() - 1;
				SelectionChanged?.Invoke(this, new DeckEventArgs(deck));
			}
			DeckAdded?.Invoke(this, EventArgs.Empty);
		}

		/// <summary>
		/// Add a new deck to the combo box.
		/// </summary>
		/// <param name="name">Name of the newly created deck.</param>
		/// <param name="select">If set to <c>true</c> this deck will be active in the combobox.</param>
		public void Add(string name, bool select = true)
		{
			var newDeck = new NamedDeck { Name = name };
			Add(newDeck, select);
		}

		/// <summary>
		/// Displays the Add Deck dialog.
		/// </summary>
		public void AddWithDialog()
		{
			var parent = (Window)Toplevel;
			var dial = new MessageDialog(parent,
																	 DialogFlags.Modal,
																	 MessageType.Question,
																	 ButtonsType.OkCancel,
																	 "Enter the name of the deck.");
			var input = new Entry("New deck")
			{
				IsEditable = true,
				HasFrame = true
			};
			var box = new HBox();
			box.PackStart(new Label("Name: "));
			box.PackEnd(input, true, true, 0);
			input.GrabFocus();
			input.Activated += delegate { dial.Respond(ResponseType.Ok); };
			dial.VBox.PackEnd(box, true, true, 0);
			dial.ShowAll();
			var resp = dial.Run();
			if (resp == -5) // For some reason, -5 is the Ok response
			{
				var name = input.Text;
				Add(name);
			}
			dial.Destroy();
		}

		/// <summary>
		/// Gets the active deck of the combo box.
		/// </summary>
		public NamedDeck ActiveDeck
		{
			get
			{
				if (_deckComboBox.GetActiveIter(out var activeIter))
				{
					var activeDeck = (NamedDeck)_deckStore.GetValue(activeIter, 0);
					return activeDeck;
				}
				return null;
			}
		}

		/// <summary>
		/// Occurs when a deck is added.
		/// </summary>
		public event EventHandler DeckAdded;
		/// <summary>
		/// Occurs when the active deck is changed.
		/// </summary>
		public event EventHandler SelectionChanged;
		ListStore _deckStore;

		void OnNewButtonClicked(object sender, EventArgs e)
		{
			AddWithDialog();
		}

		void OnDeckComboBoxChanged(object sender, EventArgs e)
		{
			SelectionChanged?.Invoke(this, EventArgs.Empty);
		}

		static object[] ValueToArray(NamedDeck deck)
		{
			return new object[] { deck, deck.Name };
		}
	}
}