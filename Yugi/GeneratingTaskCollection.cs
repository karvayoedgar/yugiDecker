﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Yugi
{
	public class GeneratingTaskCollection
	{
		public GeneratingTaskCollection()
		{
			_tasks = new List<Task>();
		}

		public Task Add(Action exec)
		{
			var newTask = new Task(exec);
			_tasks.Add(newTask);
			return newTask;
		}

		public Task AddAndRun(Action exec)
		{
			var ret = Add(exec);
			ret.Start();
			return ret;
		}

		readonly List<Task> _tasks;
	}
}