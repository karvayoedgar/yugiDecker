﻿using System;
using System.Collections.Generic;
using System.Linq;
using CE.Collections;
using Gtk;
using Newtonsoft.Json;
using Yugi;
using YugiDecker;


/// <summary>
/// Main window.
/// </summary>
public partial class MainWindow : Window
{
	public CardCollection AllCards { get; private set; }

	/// <summary>
	/// </summary>
	public MainWindow() : base(WindowType.Toplevel)
	{
		_tasks = new GeneratingTaskCollection();

		LoadDB();

		Build();
		_cardList = new NodeStore(typeof(CardEntry));
		//_currentDeckView = new NodeView(_cardList);
		_currentDeckView.NodeStore = _cardList;

		var nameCol = new TreeViewColumn("Name", new CellRendererText(), "text", 0);
		_currentDeckView.AppendColumn(nameCol);

		_currentDeckView.ShowAll();
	}

	/// <summary>
	/// Populates the card list from the current deck.
	/// </summary>
	public void PopulateCardList()
	{
		_cardList.Clear();
		var deck = DeckSelector.ActiveDeck;
		foreach (var card in deck)
			_cardList.AddNode(new CardEntry(card));
		ShowAll();
	}

	/// <summary>
	/// Ons the delete event.
	/// </summary>
	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}

	/// <summary>
	/// Action: exit
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	protected void OnDeleteActionActivated(object sender, System.EventArgs e)
	{
		Application.Quit();
	}

	protected void OnDeckSelectorSelectionChanged(object sender, System.EventArgs e)
	{
		PopulateCardList();
	}

	protected void OnGenerateDeckButtonClicked(object sender, System.EventArgs e)
	{
		var r = new Random();
		var name = r.Next().ToString();
		var gen = GenerateNewDeck(LoadDefaultTrunk());
		_tasks.AddAndRun(gen.Iterate);
		Console.WriteLine("Starting the generation of tehe new deck: {0}", name);

		/*
		 * For debug purpose only
		 */

		gen.Breakthrough += delegate
		{
			Console.WriteLine("BT: fittest deck value is {0}", gen.CurrentFittestValue);
		};
	}

	Bag<ICard> LoadDefaultTrunk()
	{
		var jsonStr = System.IO.File.ReadAllText("Trunk.json");
		var myBagArray = JsonConvert.DeserializeObject<string[]>(jsonStr);
		var myBag = new Bag<ICard>();

		foreach (var c in myBagArray)
			myBag.Add(AllCards.Get(c));

		return myBag;
	}

	void LoadDB()
	{
		AllCards = CardCollection.LoadFromFile("Cards.json");


		try
		{
			Console.WriteLine("Trying to load state.");
			var jsonStr = System.IO.File.ReadAllText(STATE_FILENAME);
			var state = JsonConvert.DeserializeObject<string[][]>(jsonStr);
			var deckState = new List<Deck>();
			foreach (var z in state)
			{
				var newDeck = z.Select(w => AllCards.Get(w));
				var deckyDeck = new Deck();
				foreach (var card in newDeck)
					deckyDeck.Add(card);
				deckState.Add(deckyDeck);
			}
			// Loading states not implemented
			// optimizer.SetState(deckState);
			Console.WriteLine("Loaded state.");
			// prevFittest = optimizer.GetFittest();
		}
		catch (Exception)
		{
			var mess = new MessageDialog(
				this,
				DialogFlags.Modal,
				MessageType.Error,
				ButtonsType.Ok,
				"Unable to load state. Generating new state.");

			Console.WriteLine("Unable to load state. Generating new state.");
			//optimizer.Populate();
		}
	}

	DeckGeneratingTaskState GenerateNewDeck(Bag<ICard> myBag)
	{
		var rules = new DeckRulesSet();
		var param = GenerationParameters.Default;
		var ret = new DeckGeneratingTaskState(param, rules, myBag);

		ret.SetupNew();
		ret.Initialize();

		return ret;
		/*
		var diffDeckDict = BagComparer.BagDifference(prevFittest.cards, d.cards);
		var ordered = diffDeckDict
			.Where(z => z.Value != 0)
			.OrderBy(z => z.Key, Comparer<ICard>.Create((x, y) => string.Compare(x.Name, y.Name)));

		Console.WriteLine();

		foreach (var w in ordered)
		{
			var color = w.Value > 0 ? ConsoleColor.Green : ConsoleColor.Red;
			Console.ForegroundColor = color;
			Console.WriteLine("{0} {1}", w.Value, w.Key.Name);
		}
		Console.ResetColor();
*/
	}
	NodeStore _cardList;
	GeneratingTaskCollection _tasks;

	const string STATE_FILENAME = "State.json";
}