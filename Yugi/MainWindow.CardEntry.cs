﻿using Gtk;
using YugiDecker;

public partial class MainWindow
{
	[TreeNode]
	class CardEntry : TreeNode
	{
		[TreeNodeValue(Column = 0)]
		public string Name => Card.Name;
		public ICard Card { get; }

		public CardEntry(ICard card)
		{
			Card = card;
		}
	}
}