﻿using System.Diagnostics;
using GLib;
using Gtk;

namespace YugiDecker
{
	class MainClass
	{
		static MainWindow MainWindow;
		public static void Main()
		{
			//ExceptionManager.UnhandledException += ExceptionManager_UnhandledException;

			Application.Init();
			MainWindow = new MainWindow();
			MainWindow.Show();
			Application.Run();
		}


		static void ExceptionManager_UnhandledException(UnhandledExceptionArgs args)
		{
			Debug.WriteLine(args);
		}
	}
}