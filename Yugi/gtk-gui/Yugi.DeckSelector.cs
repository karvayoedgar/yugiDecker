
// This file has been generated by the GUI designer. Do not modify.
namespace Yugi
{
	public partial class DeckSelector
	{
		private global::Gtk.HBox hbox1;

		private global::Gtk.ComboBox _deckComboBox;

		private global::Gtk.HButtonBox hbuttonbox2;

		private global::Gtk.Button newButton;

		private global::Gtk.Button cloneButton;

		private global::Gtk.Button removeButton;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget Yugi.DeckSelector
			global::Stetic.BinContainer.Attach(this);
			this.Name = "Yugi.DeckSelector";
			// Container child Yugi.DeckSelector.Gtk.Container+ContainerChild
			this.hbox1 = new global::Gtk.HBox();
			this.hbox1.Name = "hbox1";
			this.hbox1.Spacing = 6;
			// Container child hbox1.Gtk.Box+BoxChild
			this._deckComboBox = new global::Gtk.ComboBox();
			this._deckComboBox.Name = "_deckComboBox";
			this._deckComboBox.Active = 0;
			this.hbox1.Add(this._deckComboBox);
			global::Gtk.Box.BoxChild w1 = ((global::Gtk.Box.BoxChild)(this.hbox1[this._deckComboBox]));
			w1.Position = 0;
			// Container child hbox1.Gtk.Box+BoxChild
			this.hbuttonbox2 = new global::Gtk.HButtonBox();
			this.hbuttonbox2.Name = "hbuttonbox2";
			this.hbuttonbox2.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));
			// Container child hbuttonbox2.Gtk.ButtonBox+ButtonBoxChild
			this.newButton = new global::Gtk.Button();
			this.newButton.CanFocus = true;
			this.newButton.Name = "newButton";
			this.newButton.UseUnderline = true;
			this.newButton.Label = global::Mono.Unix.Catalog.GetString("_New");
			global::Gtk.Image w2 = new global::Gtk.Image();
			w2.Pixbuf = global::Stetic.IconLoader.LoadIcon(this, "gtk-add", global::Gtk.IconSize.Menu);
			this.newButton.Image = w2;
			this.hbuttonbox2.Add(this.newButton);
			global::Gtk.ButtonBox.ButtonBoxChild w3 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox2[this.newButton]));
			w3.Expand = false;
			w3.Fill = false;
			// Container child hbuttonbox2.Gtk.ButtonBox+ButtonBoxChild
			this.cloneButton = new global::Gtk.Button();
			this.cloneButton.CanFocus = true;
			this.cloneButton.Name = "cloneButton";
			this.cloneButton.UseUnderline = true;
			this.cloneButton.Label = global::Mono.Unix.Catalog.GetString("_Clone");
			global::Gtk.Image w4 = new global::Gtk.Image();
			w4.Pixbuf = global::Stetic.IconLoader.LoadIcon(this, "gtk-copy", global::Gtk.IconSize.Menu);
			this.cloneButton.Image = w4;
			this.hbuttonbox2.Add(this.cloneButton);
			global::Gtk.ButtonBox.ButtonBoxChild w5 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox2[this.cloneButton]));
			w5.Position = 1;
			w5.Expand = false;
			w5.Fill = false;
			// Container child hbuttonbox2.Gtk.ButtonBox+ButtonBoxChild
			this.removeButton = new global::Gtk.Button();
			this.removeButton.CanFocus = true;
			this.removeButton.Name = "removeButton";
			this.removeButton.UseUnderline = true;
			this.removeButton.Label = global::Mono.Unix.Catalog.GetString("_Remove");
			global::Gtk.Image w6 = new global::Gtk.Image();
			w6.Pixbuf = global::Stetic.IconLoader.LoadIcon(this, "gtk-remove", global::Gtk.IconSize.Menu);
			this.removeButton.Image = w6;
			this.hbuttonbox2.Add(this.removeButton);
			global::Gtk.ButtonBox.ButtonBoxChild w7 = ((global::Gtk.ButtonBox.ButtonBoxChild)(this.hbuttonbox2[this.removeButton]));
			w7.Position = 2;
			w7.Expand = false;
			w7.Fill = false;
			this.hbox1.Add(this.hbuttonbox2);
			global::Gtk.Box.BoxChild w8 = ((global::Gtk.Box.BoxChild)(this.hbox1[this.hbuttonbox2]));
			w8.Position = 1;
			w8.Expand = false;
			this.Add(this.hbox1);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.Hide();
			this._deckComboBox.Changed += new global::System.EventHandler(this.OnDeckComboBoxChanged);
			this.newButton.Clicked += new global::System.EventHandler(this.OnNewButtonClicked);
		}
	}
}
