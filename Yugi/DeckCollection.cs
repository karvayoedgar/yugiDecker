﻿using System.Collections.ObjectModel;

namespace Yugi
{
	/// <summary>
	/// Collection of <see cref="NamedDeck"/>.
	/// </summary>
	public class DeckCollection : Collection<NamedDeck>
	{
	}
}