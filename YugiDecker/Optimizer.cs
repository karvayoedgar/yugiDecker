using System;
using CE.Collections;
using ECM;

namespace YugiDecker
{
	public static class Optimizer
	{
		public const int StartingIters = 60;
		public static ICard AddCard (Deck deck, Bag<ICard> trunk, DeckRulesSet rules)
		{
			// remove deck from trunk
			// TODO: ctor to clone bags
			// TODO: '-' operator

			var unusedTrunk = rules.GetLegalCards (trunk, deck);
			var c = MetricAdvicer.Suggest (deck, unusedTrunk);
			deck.Add (c);
			return c;
		}

		public static ICard RemoveCard (Deck deck)
		{
			var c = MetricAdvicer.SuggestLowest (deck);
			deck.Remove (c);
			return c;
		}

		public static Deck Optimize (Bag<ICard> trunk, DeckRulesSet rules, GenerationParameters param)
		{
			var ret = new Deck { Params = param };
			Optimize (ret, trunk, rules);
			return ret;
		}

		public static Deck Optimize (Bag<ICard> trunk, DeckRulesSet rules)
		{
			return Optimize (trunk, rules, GenerationParameters.Default);
		}

		public static void Optimize (Deck deck, Bag<ICard> trunk, DeckRulesSet rules)
		{
			ICard newCard;
			double oldVal = deck.GetValue ();
			bool exitFlag = false;

			var cycleVal = double.NegativeInfinity;
			while (!exitFlag)
			{
				while (deck.Count < StartingIters)
				{
					newCard = AddCard (deck, trunk, rules);
					if (newCard.Name == "Chaos Sorcerer")
					{
						deck.Remove (newCard);
						newCard = AddCard (deck, trunk, rules);
					}
					var newVal = deck.GetValue ();
					Console.WriteLine ("Individual pick: {0}\t{1}", newCard, newVal - oldVal);
					oldVal = newVal;
				}

				// Cleaning
				while (deck.Count > rules.MinCards)
				{
					newCard = RemoveCard (deck);
					var newVal = deck.GetValue ();
					Console.WriteLine ("Removing:        {0}\t{1}", newCard, newVal - oldVal);
					oldVal = newVal;
				}
				var cVal = deck.GetValue ();

				Console.WriteLine ("Current deck value: {0}", cVal);
				if (cycleVal < cVal)
					cycleVal = cVal;
				else
					exitFlag = true;
			}
			// Big starting state

			// Fill min cards
			while (deck.Count < rules.MinCards)
			{
				newCard = AddCard (deck, rules.GetLegalCards (trunk, deck), rules);
				var newVal = deck.GetValue ();
				Console.WriteLine ("Mandatory: {0}\t{1}", newCard, newVal - oldVal);
				oldVal = newVal;
			}
			// The rest
			while (deck.Count < rules.MaxCards && deck.Suggest (rules.GetLegalCards (trunk, deck), out newCard))
			{
				deck.Add (newCard);
				var newVal = deck.GetValue ();
				Console.WriteLine ("Optional: {0}\t{1}", newCard, newVal - oldVal);
				oldVal = newVal;
			}
		}
	}
}