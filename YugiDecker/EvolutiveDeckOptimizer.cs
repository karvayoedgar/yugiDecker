﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CE.Collections;

namespace YugiDecker
{
	public class EvolutiveDeckOptimizer
	{
		public IEnumerable<Deck> Pool => _pool.Decks;
		public int PoolCount => _pool.Decks.Count;

		public int HighCardCount { get; }

		public EvolutiveDeckOptimizer (int highCardCount, DeckRulesSet rules, Bag<ICard> trunk)
		{
			HighCardCount = highCardCount;
			_pool = new DeckPool ();
			_r = new Random ();
			_rules = rules;
			_trunk = trunk;
		}

		public Deck GetFittest () => _pool.GetFittest ();
		public Deck [] GetSortedDeckArray () => _pool.GetSortedDeckArray ();

		public void Populate ()
		{
			_pool.Clear ();
			for (int i = 0; i < HighCardCount; i++)
			{
				Console.WriteLine (i);
				var newDeck = GenerateNewDeck ();
				_pool.Add (newDeck);
			}
		}

		public void SetState (IEnumerable<Deck> pool)
		{
			_pool = new DeckPool ();
			foreach (var d in pool)
				_pool.Add (d);
		}

		public void Iterate (int times)
		{
			for (int i = 0; i < times; i++) IterateOnce ();
		}

		public void Iterate () => IterateOnce ();


		public List<List<string>> DeckCardNames ()
		{
			var ret = new List<List<string>> ();
			foreach (var deck in _pool.Decks)
				ret.Add (deck.ToNameList ());

			return ret;
		}

		void IterateOnce ()
		{
			RemoveUnfitted ();
			CloneMutate ();
			Debug.WriteLine ("Gen++");
		}

		void RemoveUnfitted ()
		{
			_pool.RemoveBelowMid ();
		}

		Deck GenerateNewDeck () => GenerateNewDeck ((_rules.MaxCards + _rules.MinCards) / 2);
		Deck GenerateNewDeck (int count)
		{
			var ret = new Deck ();
			while (ret.Count < count)
			{
				var newCard = PickRandom (_trunk, _r);
				if (_rules.IsCardLegal (newCard, ret) &&
					 _trunk [newCard] > ret.CountOfType (newCard))
					ret.Add (newCard);
			}
			return ret;
		}

		void CloneMutate ()
		{
			while (PoolCount < HighCardCount)
				CloneMutateDeck ();
		}

		void CloneMutateDeck ()
		{
			var deck = PickRandom (_pool.Decks, _r);
			var newDeck = CloneMutateDeck (deck);

			_pool.Add (newDeck);
		}

		Deck CloneMutateDeck (Deck deck)
		{
			var ret = deck.Clone ();
			while (deck.Count < _rules.MaxCards &&
					 _r.NextDouble () < _addCardChance)
			{
				var newCard = PickRandom (_trunk, _r);
				if (_rules.IsCardLegal (newCard, deck) &&
					 _trunk [newCard] > deck.CountOfType (newCard))
					deck.Add (newCard);
			}

			while (deck.Count > _rules.MinCards &&
				 _r.NextDouble () < _removeCardChance)
			{
				var remCard = PickRandom (deck, _r);
				deck.Remove (remCard);
			}

			return ret;
		}

		DeckPool _pool;
		Random _r;
		DeckRulesSet _rules;
		Bag<ICard> _trunk;

		double _addCardChance = 0.5;
		double _removeCardChance = 0.5;

		static T PickRandom<T> (ICollection<T> cards, Random r)
		{
			var count = cards.Count;
			foreach (var z in cards)
			{
				if (r.NextDouble () * count < 1)
					return z;
				count--;
			}
			throw new Exception ();
		}

		class DeckPool
		{
			public ICollection<Deck> Decks => _person.Keys;
			public DeckPool ()
			{
				_person = new Dictionary<Deck, double> ();
			}

			public void Clear ()
			{
				_person.Clear ();
			}

			public void Add (Deck deck)
			{
				_person.Add (deck, deck.GetValue ());
			}

			public void RemoveBelowMid ()
			{
				var mid = Mid ();
				foreach (var z in _person.Where (z => z.Value < mid).ToArray ())
					_person.Remove (z.Key);
			}

			public Deck GetFittest ()
			{
				return _person.Aggregate ((arg1, arg2) => arg1.Value < arg2.Value ? arg2 : arg1).Key;
			}

			public Deck [] GetSortedDeckArray ()
			{
				return _person.OrderBy (z => z.Value).Select (z => z.Key).ToArray ();
			}

			double Mid () => (_person.Select (z => z.Value).Max () + _person.Select (z => z.Value).Min ()) / 2d;

			readonly Dictionary<Deck, double> _person;
		}
	}
}
