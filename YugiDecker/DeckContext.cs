using AdvMath.Fórmula;
using System.Linq;
using System;

namespace YugiDecker
{
	public class DeckContext : IEvalContext<float>
	{
		public readonly Deck Deck;
		public IFórmula<float> GetFunc (string str)
		{
			switch (str)
			{
			case "greater":
				return new FuncFórmula<float> ((ct, args) => args [0] > args [1] ? 1 : 0);
			case "pow":
				return new FuncFórmula<float> ((ct, args) => (float)Math.Pow (args [0], args [1]));
			case "generating":
				// generating
				// 0: limit-value, 1: zero-value, 2: speed, 3: generator
				return new FuncFórmula<float> ((ct, args) =>
					(float)(args [0] - (args [0] - args [1]) * (Math.Pow (args [2], args [3]))));
			case "max":
				return new FuncFórmula<float> ((ct, args) => args.Max ());
			case "min":
				return new FuncFórmula<float> ((ct, args) => args.Min ());
			}

			if (str.Substring (0, 24) == "count.monster.level.tag.")
			{
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
				var tagName = str.Substring (24);
				return new FuncFórmula<float> ((ct, args) =>
							Deck.OfType<MonsterCard> ()
								 .Where (z => z.Level == args [0])
								 .Sum (z => z.TagValue (tagName)) / Deck.Count);
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
			}

			throw new Exception (string.Format ("Invalid func name: {0}", str));
		}

		public float GetVar (string str)
		{
			// count meaning density
			str = str.Replace ('_', ' ');
			str = str.Replace ('^', '-');
			switch (str)
			{
			case "count.monster":
				var r = (float)Deck.Count (c => c.Tags.Contains ("MonsterCard")) / Deck.Count;
				return r;
			case "count.warrior":
				return (float)Deck.OfType<MonsterCard> ().Count (c => c.Type == "Warrior") / Deck.Count;
			case "count.monster.low.normal":
				return Deck.OfType<MonsterCard> ().Count (z => z.ReqTributes == 0 && !(z is MonsterEffectCard)) / Deck.Count;
			case "count.monster.normal":
				return Deck.OfType<MonsterCard> ().Count (z => !(z is MonsterEffectCard)) / Deck.Count;
			case "count.monster.low":
				return (float)Deck.OfType<MonsterCard> ().Count (c => c.Level <= 4) / Deck.Count;
			case "count.monster.tribute.2":
				return (float)Deck.OfType<MonsterCard> ().Count (c => c.Level > 6) / Deck.Count;
			case "count.monster.tribute.1":
				return (float)Deck.OfType<MonsterCard> ().Count (c => c.Level <= 6 && c.Level > 4) / Deck.Count;
			case "count.deck":
				return Deck.Count;
			case "count.ToVoid":
				return Deck.Sum (z =>
									  z.TagValue ("GraveyardToVoid") +
									  z.TagValue ("HandToVoid") +
									  z.TagValue ("FieldToVoid") +
									  z.TagValue ("DeckToVoid"))
							  / Deck.Count;
			}

			if (str.Substring (0, 10) == "count.tag.")
			{
				var tagName = str.Substring (10);
				return Deck.Sum (c => c.TagValue (tagName)) / Deck.Count;
			}

			if (str.StartsWith ("count.monster.tribute.0.tag.", StringComparison.CurrentCulture))
			{
				var arg = str.Substring (28);
				return Deck.OfType<MonsterCard> ().Where (c => c.ReqTributes == 0).Sum (c => c.TagValue (arg)) / Deck.Count;
			}

			if (str.StartsWith ("count.monster.tribute.1.tag.", StringComparison.CurrentCulture))
			{
				var arg = str.Substring (28);
				return Deck.OfType<MonsterCard> ().Where (c => c.ReqTributes == 1).Sum (c => c.TagValue (arg)) / Deck.Count;
			}

			if (str.StartsWith ("count.monster.tribute.2.tag.", StringComparison.CurrentCulture))
			{
				var arg = str.Substring (28);
				return Deck.OfType<MonsterCard> ().Where (c => c.ReqTributes == 2).Sum (c => c.TagValue (arg)) / Deck.Count;
			}

			if (str.Substring (0, 14) == "count.monster.")
			{
				var monName = str.Substring (14);
				return (float)Deck.CountName (monName) / Deck.Count;
			}

			if (str.Substring (0, 18) == "count.monstertype.")
			{
				var monType = str.Substring (18);
				return (float)Deck.CountType (monType) / Deck.Count;
			}

			if (str.Substring (0, 21) == "count.monsterelement.")
			{
				var monType = str.Substring (21);
				return (float)Deck.CountElement (monType) / Deck.Count;
			}

			throw new Exception (string.Format ("Invalid var name: {0}", str));
		}

		public DeckContext (Deck deck)
		{
			Deck = deck;
		}
	}
}