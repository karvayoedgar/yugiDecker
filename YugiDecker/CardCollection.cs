﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using System;

namespace YugiDecker
{
	public class CardCollection
	{
		[JsonProperty ("Cards")]
		readonly ICard [] collection;
		public ICard Get (string name)
		{
			try
			{
				return collection.First (z => z.Name == name);
			}
			catch (System.InvalidOperationException ex)
			{
				throw new InvalidOperationException (string.Format ("Cannot find card {0} in the collection", name), ex);
			}
		}

		public int Count { get { return collection.Length; } }

		public IEnumerable<ICard> EnumerateAll ()
		{
			return collection;
		}

		static JsonSerializerSettings sets = new JsonSerializerSettings
		{
			Formatting = Formatting.Indented,
			PreserveReferencesHandling = PreserveReferencesHandling.None,
			ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
			TypeNameHandling = TypeNameHandling.Objects,
			TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
		};

		public static CardCollection LoadFromFile (string filename)
		{
			// TODO: File check
			var jsonStr = File.ReadAllText (filename);
			return JsonConvert.DeserializeObject<CardCollection> (jsonStr, sets);
		}

		[JsonConstructor]
		CardCollection (ICard [] Cards)
		{
			collection = Cards;
		}

		public CardCollection (ICard card)
		{
			collection = new ICard [1];
			collection [0] = card;
		}
	}
}