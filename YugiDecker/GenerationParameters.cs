﻿using System.IO;
using Newtonsoft.Json;

namespace YugiDecker
{
	public class PenaltyArgs
	{
		public float Penalty;
		public float Threshold;

		public PenaltyArgs (float penalty, float threshold)
		{
			Penalty = penalty;
			Threshold = threshold;
		}
	}
	public class GenerationParameters
	{
		public TagDict TagWeights;
		public PenaltyArgs [] MonsterPenalty;
		public PenaltyArgs [] MagicPenalty;
		public PenaltyArgs [] TributePenalty;

		public double MonsterMetrics;
		public readonly static GenerationParameters Default;
		public float GetCoefForCard (ICard card)
		{
			var ret = 1f;
			foreach (var tag in card.Tags)
			{
				if (TagWeights.TryGetValue (tag, out float tagWh))
					ret *= tagWh;
			}

			return ret;
		}

		static JsonSerializerSettings sets = new JsonSerializerSettings
		{
			Formatting = Formatting.Indented,
			PreserveReferencesHandling = PreserveReferencesHandling.None,
			ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
			TypeNameHandling = TypeNameHandling.Objects,
			TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
		};

		public static GenerationParameters LoadFromFile (string filename)
		{
			// TODO: File check
			var jsonStr = File.ReadAllText (filename);
			return JsonConvert.DeserializeObject<GenerationParameters> (jsonStr, sets);
		}

		[JsonConstructor]
		public GenerationParameters ()
		{
			TagWeights = new TagDict ();
		}

		static GenerationParameters ()
		{
			Default = new GenerationParameters
			{
				MonsterMetrics = 4,
				MonsterPenalty = new PenaltyArgs [2],
				MagicPenalty = new PenaltyArgs [2],
				TributePenalty = new PenaltyArgs [2]
			};
			Default.MonsterPenalty [0] = new PenaltyArgs (0.9f, 0.55f);
			Default.MonsterPenalty [1] = new PenaltyArgs (0.7f, 0.65f);
			Default.MagicPenalty [0] = new PenaltyArgs (0.95f, 0.55f);
			Default.MagicPenalty [1] = new PenaltyArgs (0.85f, 0.65f);
			Default.TributePenalty [0] = new PenaltyArgs (0.9f, 0.1f);
			Default.TributePenalty [1] = new PenaltyArgs (0.9f, 0f);
		}
	}
}