﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CE.Collections;
using ECM;
using Newtonsoft.Json;

namespace YugiDecker
{
	class MainClass
	{
		public static CardCollection AllCards;
		public static void Main (string [] args)
		{
			AllCards = CardCollection.LoadFromFile ("Cards.json");

			string jsonStr;
			if (args.Length > 0)
				jsonStr = File.ReadAllText (args [0]);
			else
				jsonStr = File.ReadAllText ("Trunk.json");

			var myBagArray = JsonConvert.DeserializeObject<string []> (jsonStr);
			var myBag = new Bag<ICard> ();

			foreach (var c in myBagArray)
				myBag.Add (AllCards.Get (c));

			var rules = new DeckRulesSet ();
			var param = args.Length > 1 ? GenerationParameters.LoadFromFile (args [1]) : GenerationParameters.Default;

			var optimizer = new EvolutiveDeckOptimizer (2000, rules, myBag);
			Deck prevFittest;
			try
			{
				Console.WriteLine ("Trying to load state.");
				jsonStr = File.ReadAllText (STATE_FILENAME);
				var state = JsonConvert.DeserializeObject<string [] []> (jsonStr);
				var deckState = new List<Deck> ();
				foreach (var z in state)
				{
					var newDeck = z.Select (w => AllCards.Get (w));
					var deckyDeck = new Deck ();
					foreach (var card in newDeck)
						deckyDeck.Add (card);
					deckState.Add (deckyDeck);
				}
				optimizer.SetState (deckState);
				Console.WriteLine ("Loaded state.");
				prevFittest = optimizer.GetFittest ();
			}
			catch (Exception)
			{
				Console.WriteLine ("Unable to load state. Generating new state.");
				optimizer.Populate ();
				prevFittest = new Deck ();
			}

			Console.Write ("Minutes: ");
			var minInv = double.Parse (Console.ReadLine () ?? "1");
			var endTime = DateTime.Now + TimeSpan.FromMinutes (minInv);
			//var d = Optimizer.Optimize (myBag, rules, param);
			Deck d = optimizer.GetFittest ();
			Console.WriteLine ("End time: {0:hh:mm:ss}", endTime);

			var lastValue = double.NegativeInfinity;
			while (DateTime.Now < endTime)
			{
				var sortedDeck = optimizer.GetSortedDeckArray ();
				var nowHighestDeck = sortedDeck [sortedDeck.Length - 1];
				d = nowHighestDeck;
				var leastFit = sortedDeck [0];
				var currValue = d.GetValue ();
				Console.Write ("\rDiameter: {0}", currValue - leastFit.GetValue ());
				if (currValue - lastValue > 0.00001d)
				{
					lastValue = currValue;
					Console.WriteLine ("\r[{1:hh:mm:ss}] Current deck score: {0}", currValue, DateTime.Now);
				}
				optimizer.Iterate (1);
			}

			d = optimizer.GetFittest ();

			var diffDeckDict = BagComparer.BagDifference (prevFittest.cards, d.cards);
			var ordered = diffDeckDict
				.Where (z => z.Value != 0)
				.OrderBy (z => z.Key, Comparer<ICard>.Create ((x, y) => string.Compare (x.Name, y.Name)));

			Console.WriteLine ();

			foreach (var w in ordered)
			{
				var color = w.Value > 0 ? ConsoleColor.Green : ConsoleColor.Red;
				Console.ForegroundColor = color;
				Console.WriteLine ("{0} {1}", w.Value, w.Key.Name);
			}
			Console.ResetColor ();


			// Display results
			ICard maxValueCard = null;
			foreach (var card in d.GetBag ().OrderBy (z => z.Key.Name))
			{
				if (maxValueCard == null) maxValueCard = card.Key;
				Console.ForegroundColor = card.Key.DisplayColor;
				var val = d.GetValueOf (card.Key);
				Console.WriteLine ("{1}x  {0,-50}\t({2})", card.Key.Name, card.Value, val);
				if (d.GetValueOf (maxValueCard) < val) maxValueCard = card.Key;
			}

			Console.ResetColor ();
			Console.WriteLine ("\nKey card: {0}", maxValueCard.Name);

			Console.WriteLine ("Count:    {0}", d.Count);
			Console.WriteLine ("Magic:    {0} / {1:P2}", d.OfType<MagicCard> ().Count (), d.OfType<MagicCard> ().Count () / (float)d.Count);
			Console.WriteLine ("Monsters: {0} / {1:P2}", d.OfType<MonsterCard> ().Count (), d.OfType<MonsterCard> ().Count () / (float)d.Count);
			Console.WriteLine ();
			Console.WriteLine ("Spell:    {0} ", d.OfType<MagicCard> ().Count (z => z.Type == MagicType.Spell));
			Console.WriteLine ("Trap:     {0} ", d.OfType<MagicCard> ().Count (z => z.Type == MagicType.Trap));
			Console.WriteLine ("Effect:   {0} ", d.OfType<MonsterEffectCard> ().Count ());
			Console.WriteLine ("NoTrib:   {0} ", d.OfType<MonsterCard> ().Count (z => z.Level <= 4));
			Console.WriteLine ("1-Trib:   {0} ", d.OfType<MonsterCard> ().Count (z => z.Level <= 6 && z.Level > 4));
			Console.WriteLine ("2-Trib:   {0} ", d.OfType<MonsterCard> ().Count (z => z.Level > 6));

			// flavor
			var tags = new HashSet<string> ();
			foreach (var c in d)
				tags.UnionWith (c.Tags);

			var tagBag = new Dictionary<string, float> ();
			foreach (var tag in tags)
			{
				var tot = d.Sum (z => z.TagValue (tag));
				tagBag.Add (tag, tot);
			}

			foreach (var tag in tagBag.OrderByDescending (z => z.Value))
				Console.WriteLine ("{0,-20}: {1,3}", tag.Key, tag.Value);

			// store pool in file
			var stateJson = JsonConvert.SerializeObject (optimizer.DeckCardNames ());
			File.WriteAllText (STATE_FILENAME, stateJson);
		}

		const string STATE_FILENAME = "State.json";
	}
}