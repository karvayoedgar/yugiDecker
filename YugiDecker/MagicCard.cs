using AdvMath.Fórmula;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace YugiDecker
{
	public enum MagicType { Spell, Trap }
	/// <summary>
	/// Magic or trap card
	/// </summary>
	public class MagicCard : Card, IMagicCard
	{
		[JsonConverter (typeof (StringEnumConverter))]
		public MagicType Type { get; set; }

		public override System.ConsoleColor DisplayColor
		{
			get
			{
				switch (Type)
				{
				case MagicType.Spell: return System.ConsoleColor.Green;
				case MagicType.Trap: return System.ConsoleColor.Magenta;
				}
				throw new System.Exception ();
			}
		}
		public string ValueFml { get; set; }

		public float GetValue (DeckContext context, GenerationParameters param)
		{
			var fml = new Fórmula (ValueFml);
			return fml.Evaluar (context);
		}
		protected override float InherentTagValue (string tag)
		{
			if (tag == Type.ToString ()) return 1;
			return base.InherentTagValue (tag);
		}

		protected override List<string> InherentTags ()
		{
			var ret = new List<string> ();
			ret.Add (Type.ToString ());
			return ret;
		}

		public override float GetValue (Deck deck, GenerationParameters param)
		{
			return GetValue (deck.Context, param);
		}
	}
}