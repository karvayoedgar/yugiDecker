using System.Collections.Generic;
using System;

namespace YugiDecker
{
	public interface ICard
	{
		string Name { get; }

		ConsoleColor DisplayColor { get; }

		float TagValue (string tag);

		IEnumerable<string> Tags { get; }

		float GetValue (Deck deck, GenerationParameters param);
	}
}