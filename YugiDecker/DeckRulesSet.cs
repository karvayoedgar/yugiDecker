using System.Collections.Generic;
using System.Linq;
using CE.Collections;

namespace YugiDecker
{
	public class DeckRulesSet
	{
		public int MaxCardsType = 3;
		public int MaxCards = 60;
		public int MinCards = 40;

		public bool IsCardLegal (ICard card, Deck deck)
		{
			// Excludes global limitations
			return deck.CountOfType (card) < MaxCardsType && !card.Tags.Contains ("Forbidden");
		}
		public Bag<ICard> GetLegalCards (Bag<ICard> trunk, Deck currDeck)
		{
			var ret = new Bag<ICard> ();
			// TODO: Make a Type getter in Bag
			foreach (var c in (IEnumerable<KeyValuePair<ICard, int>>)trunk)
			{
				if (IsCardLegal (c.Key, currDeck) && c.Value > currDeck.CountOfType (c.Key))
					ret.Add (c.Key);
			}

			return ret;
		}
	}
}