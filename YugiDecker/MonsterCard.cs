using System;
using System.Collections.Generic;
using AdvMath.Fórmula;
using System.Linq;

namespace YugiDecker
{
	public class TagDict : Dictionary<string, float>
	{ }
	public abstract class Card : ICard
	{
		public string Name { get; set; }
		public TagDict Tags { get; set; }

		public abstract ConsoleColor DisplayColor { get; }

		IEnumerable<string> ICard.Tags { get { return Tags.Keys.Union (InherentTags ()); } }
		public float TagValue (string tag)
		{
			float ret = InherentTagValue (tag);
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
			if (ret != 0) return ret;
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
			return Tags.TryGetValue (tag, out ret) ? ret : 0;
		}

		public abstract float GetValue (Deck deck, GenerationParameters param);

		protected virtual float InherentTagValue (string tag)
		{
			return 0;
		}

		protected abstract List<string> InherentTags ();

		public override string ToString ()
		{
			return Name;
		}
	}

	public class MonsterEffectCard : MonsterCard
	{
		public override ConsoleColor DisplayColor { get { return ConsoleColor.Yellow; } }
		public string ValueFml { get; set; }

		protected override float InherentTagValue (string tag)
		{
			if (tag == "MonsterEffect") return 1;
			if (tag == "TributeMeasure")
			{
				switch (ReqTributes)
				{
				case 0: return 1f;
				case 1: return -2f;
				case 2: return -3f;
				case 3: return -6f;
				default:
					throw new Exception ();
				}
			}
			return base.InherentTagValue (tag);
		}
		protected override List<string> InherentTags ()
		{
			var ret = base.InherentTags ();
			ret.Add ("MonsterEffect");
			ret.Add ("TributeMeasure");
			return ret;
		}
		public override float GetValue (Deck deck, GenerationParameters param)
		{
			var fml = new Fórmula (ValueFml);
			return fml.Evaluar (deck.Context) + base.GetValue (deck, deck.Params);
		}
	}
	public class MonsterCard : Card
	{
		public int Level;

		public int ReqTributes
		{ get { return Level <= 4 ? 0 : (Level <= 6 ? 1 : 2); } }

		public override ConsoleColor DisplayColor { get { return ConsoleColor.White; } }

		/// <summary>
		/// Gets the inherent tag-value for a given tag (zero meaning this is not a inherent tag)
		/// </summary>
		/// <param name="tag">Tag.</param>
		protected override float InherentTagValue (string tag)
		{
			return tag == "type:" + Type ||
					 tag == "element:" + Element ||
					 tag == "Monster" ? 1 : 0;
		}

		public int Attack { get; set; }
		public int Defense { get; set; }

		public string Type;
		public string Element;

		protected override List<string> InherentTags ()
		{
			return new List<string> (new [] { "type:" + Type, "element:" + Element, "Monster" });
		}

		public override float GetValue (Deck deck, GenerationParameters param)
		{
			return (float)Math.Pow (Math.Pow (Attack, param.MonsterMetrics) + Math.Pow (Defense, param.MonsterMetrics), 1 / param.MonsterMetrics);
		}
	}
}