namespace YugiDecker
{
	public static class TagConsts
	{
		public const string ChangeAttDef = "ChangeAttDef";
		public const string MagicCard = "MagicCard";
		public const string MonsterCard = "MonsterCard";
	}
}