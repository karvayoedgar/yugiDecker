﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AdvMath.Fórmula;
using CE.Collections;
using ECM;

namespace YugiDecker
{
	public class Deck : IEnumerable<ICard>, ICollectionMetrics<ICard>
	{
		public DeckContext Context;
		public GenerationParameters Params = GenerationParameters.Default;
		internal readonly Bag<ICard> cards;

		public int Count { get { return cards.Count; } }

		public IEnumerable<KeyValuePair<ICard, int>> GetBag ()
		{
			return cards;
		}

		public int CountOfType (ICard card)
		{
			return cards [card];
		}

		public int CountName (string str)
		{
			var en = cards as IEnumerable<KeyValuePair<ICard, int>>;
			try
			{
				return en.First (z => z.Key.Name == str).Value;
			}
			catch
			{
				return 0;
			}
		}

		public int CountType (string type)
		{
			var en = cards as IEnumerable<KeyValuePair<ICard, int>>;
			return en.Count (z => z.Key is MonsterCard && ((MonsterCard)z.Key).Type == type);
		}

		public int CountElement (string element)
		{
			var en = cards as IEnumerable<KeyValuePair<ICard, int>>;
			return en.Count (z => z.Key is MonsterCard && ((MonsterCard)z.Key).Element == element);
		}

		public List<string> ToNameList ()
		{
			return new List<string> (((IEnumerable<ICard>)cards).Select (z => z.Name));
		}

		bool ICollection<ICard>.IsReadOnly { get { return false; } }

		IEnumerator<ICard> IEnumerable<ICard>.GetEnumerator ()
		{
			return ((IEnumerable<ICard>)cards).GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return ((IEnumerable<ICard>)cards).GetEnumerator ();
		}

		public double GetSumTag (string tag)
		{
			return this.Sum (z => z.TagValue (tag));
		}
		public double GetValue ()
		{
			if (Count == 0) return 0;
			var sum = 0d;
			// Return the mean value of the cards
			// Add the preference weight here
			foreach (var card in (IEnumerable<KeyValuePair<ICard, int>>)cards)
				sum += card.Value * card.Key.GetValue (this, Params) * Params.GetCoefForCard (card.Key);

			var mean = sum / cards.Count;

			var monCount = cards.OfType<MonsterCard> ().Count ();
			var magCount = cards.OfType<MagicCard> ().Count ();

			foreach (var pen in Params.MonsterPenalty)
				if (cards.Count * pen.Threshold < monCount)
					mean *= pen.Penalty;
			foreach (var pen in Params.MagicPenalty)
				if (cards.Count * pen.Threshold < magCount)
					mean *= pen.Penalty;


			// tribute penalties
			var tribMeasure = GetSumTag ("TributeMeasure") / Count;
			foreach (var pen in Params.TributePenalty)
				if (tribMeasure < pen.Threshold)
					mean *= pen.Penalty;

			return mean;
		}

		public Dictionary<string, IFórmula<float>> DeckFuncs;

		public void Add (ICard item)
		{
			if (item == null)
				throw new ArgumentNullException (nameof (item));
			cards.Add (item);
		}

		void ICollection<ICard>.Clear ()
		{
			cards.Clear ();
		}

		bool ICollection<ICard>.Contains (ICard item)
		{
			return cards.Contains (item);
		}

		void ICollection<ICard>.CopyTo (ICard [] array, int arrayIndex)
		{
			throw new Exception ();
		}

		public bool Remove (ICard item)
		{
			return cards.Remove (item);
		}

		public Deck Clone ()
		{
			var ret = new Deck ();
			ret.cards.Add (cards);
			return ret;
		}

		public Deck ()
		{
			cards = new Bag<ICard> ();
			Context = new DeckContext (this);
		}
	}
}