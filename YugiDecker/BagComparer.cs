﻿using System.Collections.Generic;
using CE.Collections;

namespace YugiDecker
{
	/// <summary>
	/// Bag comparer.
	/// </summary>
	public static class BagComparer
	{
		/// <summary>
		/// Gets the difference between two bags.
		/// </summary>
		/// <param name="before">Before.</param>
		/// <param name="after">After.</param>
		public static Dictionary<T, int> BagDifference<T> (Bag<T> before, Bag<T> after)
		{
			var ret = after.ToDictionary ();
			foreach (var z in (IEnumerable<KeyValuePair<T, int>>)before)
			{
				try { ret [z.Key] -= z.Value; }
				catch (KeyNotFoundException) { ret.Add (z.Key, -z.Value); }
			}
			return ret;
		}
	}
}