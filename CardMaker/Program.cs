﻿using System.Collections.Generic;
using YugiDecker;
using Newtonsoft.Json;

namespace CardMaker
{
	class MainClass
	{
		public static void Main ()
		{
			var card = new MagicCard
			{
				Name = "Banner of the courage",
				Type = MagicType.Trap,
				ValueFml = "200 * count.monster",
				Tags = new TagDict ()
			};
			card.Tags.Add ("Tickes", 2.5f);
			var lst = new CardCollection (card);
			JsonSerializerSettings sets = new JsonSerializerSettings
			{
				Formatting = Formatting.Indented,
				PreserveReferencesHandling = PreserveReferencesHandling.None,
				ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
				TypeNameHandling = TypeNameHandling.Objects,
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
			};
			var op = JsonConvert.SerializeObject (lst, sets);
			System.Console.WriteLine (op);
			var deck = new Deck ();
			deck.Add (card);
			var val = deck.GetValue ();
			System.Console.WriteLine (val);
		}
	}
}