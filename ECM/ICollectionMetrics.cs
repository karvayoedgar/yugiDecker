﻿using System.Collections.Generic;
namespace ECM
{
	public interface ICollectionMetrics<T> : ICollection<T>
	{
		double GetValue ();
	}
}