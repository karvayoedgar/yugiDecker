﻿using System.Collections.Generic;
namespace ECM
{
	public static class MetricAdvicer
	{
		public static T Suggest<T> (this ICollectionMetrics<T> coll, IEnumerable<T> full)
		{
			T ret;
			Suggest (coll, full, out ret);
			return ret;
		}

		public static T SuggestLowest<T> (this ICollectionMetrics<T> coll)
		{
			T ret;
			SuggestLowest (coll, out ret);
			return ret;
		}

		public static bool Suggest<T> (this ICollectionMetrics<T> coll, IEnumerable<T> full, out T sugItem)
		{
			var currMaxItem = default (T);
			var currMaxValue = double.NegativeInfinity;
			var prevValue = coll.GetValue ();
			foreach (var t in full)
			{
				coll.Add (t);
				var newVal = coll.GetValue ();
				if (newVal >= currMaxValue)
				{
					currMaxValue = newVal;
					currMaxItem = t;
				}
				coll.Remove (t);
			}
			sugItem = currMaxItem;

			return currMaxValue > prevValue;
		}

		public static bool SuggestLowest<T> (this ICollectionMetrics<T> coll, out T sugItem)
		{
			var currMaxItem = default (T);
			var currMaxValue = double.PositiveInfinity;
			var prevValue = coll.GetValue ();
			foreach (var t in coll)
			{
				coll.Add (t);
				var newVal = coll.GetValue ();
				if (newVal <= currMaxValue)
				{
					currMaxValue = newVal;
					currMaxItem = t;
				}
				coll.Remove (t);
			}
			sugItem = currMaxItem;

			return currMaxValue < prevValue;
		}

		public static float GetValueOf<T> (this ICollectionMetrics<T> coll, T obj)
		{
			if (!coll.Contains (obj))
				throw new System.Exception ("The item should be in the collection.");

			var oldVal = coll.GetValue ();
			coll.Remove (obj);
			var newVal = coll.GetValue ();
			coll.Add (obj);

			return (float)(oldVal - newVal);
		}
	}
}